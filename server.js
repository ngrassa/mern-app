const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const client = require('prom-client');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 5000;

app.use(cors({
  origin: 'http://my-first-app.local'
}));
// Create a Registry to register the metrics
const register = new client.Registry();

// Enable the collection of default metrics
client.collectDefaultMetrics({ register });

// Define a route to expose the metrics
app.get('/metrics', async (req, res) => {
  try {
    res.set('Content-Type', register.contentType);
    res.end(await register.metrics());
  } catch (err) {
    res.status(500).end(err);
  }
});

// Connect to MongoDB
mongoose.connect('mongodb+srv://justbiel:Gg-313715@cluster0.tdihg3c.mongodb.net/Text?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('MongoDB connected'))
  .catch(err => console.log(err));

// Define schema and model
const TestSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true
  }
});

const Test = mongoose.model('Test', TestSchema);

// Middleware
app.use(bodyParser.json());

// Enable CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'); // Allow requests from any origin
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); // Allow specified HTTP methods
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept'); // Allow specified headers
  next();
});

// Respond to GET requests on /api/tests
app.get('/api/check', (req, res) => {
  res.json({ message: 'Backend server is working fine!' });
});

// Routes
app.post('/api/tests', (req, res) => {
  const newTest = new Test({
    text: req.body.text
  });

  newTest.save()
    .then(test => res.json(test))
    .catch(err => res.status(400).json({ error: err.message }));
});

// Start server
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));