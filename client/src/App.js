import React, { useState } from 'react';
import axios from 'axios';

function App() {
  const [textInput, setTextInput] = useState('');

  const handleAdd = async () => {
    try {
      console.log('Text to be submitted:', textInput); // Log the text before making the request
      const response = await axios.post('http://my-first-app.local/api/tests', { text: textInput });
      console.log('Text submitted successfully:', response.data);
      setTextInput('');
      window.location.reload();
    } catch (error) {
      console.error('Error submitting text:', error);
    }
  };

  return (
    <div className="App">
      <input
        type="text"
        value={textInput}
        onChange={(e) => setTextInput(e.target.value)}
        placeholder="Enter text..."
      />
      {/* Change the button text to "Add" and wire up the onClick event */}
      <button onClick={handleAdd}>Add</button>
    </div>
  );
}

export default App;
