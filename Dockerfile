# Use a base image with Node.js 20.4.0 pre-installed
FROM node:20.4.0-alpine

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package.json package-lock.json ./

# Install production dependencies
RUN npm install --production

# Copy the rest of the application code
COPY . .

# Switch to non-root user for better security
USER node

# Expose port 5000 (if needed)
EXPOSE 5000

# Command to start the backend server
CMD ["node", "server.js"]

